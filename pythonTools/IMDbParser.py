#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

import urllib2 as u2
import re
import sys
import types
import pg

try:
   import psyco
   print 'Found working psyco installation!'
   psyco.full()
except:
   print 'You do not have a working psyco installation!'


class IMDbParser:

   def __init__(self):
      self.fields = ['Directed by', 'Writing credits', 'Cast', 'Produced by',\
      'Cinematography by', 'Film Editing by']

      self.user_rating_regex = re.compile('User Rating:</b>.*?<b>(?P<rating>[0-9]+\.[0-9]+)/10</b>',re.DOTALL)
      self.mpaa_rating_regex = re.compile('MPAA</a>:</b>(?P<rating>.*?)<br>',re.DOTALL)
      self.country_regex = re.compile('Country:.*?">(?P<country>.*?)</a>',re.DOTALL)
      self.genre_regex = re.compile('Genre:.*?(more)',re.DOTALL)
      self.genre_substring = re.compile('">(?P<target>[a-zA-Z0-9 .]+)</a>')
      self.keyword_regex = re.compile('keyword.*?>(?P<target>[a-zA-Z0-9.]+)</a>' )
      self.plot_summary_regex = re.compile('<p class=\"plotpar\">(?P<target>.*)</p>')
      self.fullcredit_id_regex = 'name/nm0(?P<target>[0-9]+)/">' 

   def get(self,id):
      self.id = id
      self.baseurl = 'http://www.imdb.com/title/tt0%06i' % id

   def parse(self):
      self.fullcredit_parser()
      self.rating_parser()
      self.keywords_parser()

   def fullcredit_parser(self):
      fullcredits_url = self.baseurl + 'fullcredits'
      site = u2.urlopen(fullcredits_url)
      fullfile = site.read()

      persons = {}

      for i in range(len(self.fields)):
         person_list = []
         
         actualField =self.fields[i]
         before =self.fields[i]
         try:
            after =self.fields[i+1]
         except:
            after = ''

         try:
            re_string = '%s</b>.*?%s</b>' % (before,after)
            actual_rx = re.compile(re_string,re.DOTALL)
            mo = actual_rx.search(fullfile)
            substring = mo.group()

            hits = self.fullcredit_id_regex.findall(substring)
            for elem in hits:
               person_list.append(elem)
            
            persons[self.fields[i]] = person_list
         except:
            persons[self.fields[i]] = []
         
      self.fullcredits = persons


   def rating_parser(self):
      credits_url = self.baseurl + ''
      site = u2.urlopen(credits_url)
      fullfile = site.read()

      # get user rating
      mo = self.user_rating_regex.search(fullfile)
      try:
         r = mo.group('rating')
         self.user_rating = r
      except:
         self.user_rating = '-1.0'
         pass

      # get MPAA rating
      mo = self.mpaa_rating_regex.search(fullfile)
      try:
         self.mpaa_rating = mo.group('rating').strip()
      except:
         self.mpaa_rating = "N/A"

      # get country
      mo = self.country_regex.search(fullfile)
      try:
         self.country = mo.group('country').strip()
      except:
         self.country = ''

      #get genres
      genre_list = []
      try:
         mo = self.genre_regex.search(fullfile)
         substring = mo.group()
         hits = self.genre_substring.findall(substring)
         for elem in hits:
            genre_list.append(elem)
      except:
         pass
       
      self.genres = genre_list


   def keywords_parser(self):
      keywords_url = self.baseurl + 'keywords'
      site = u2.urlopen(keywords_url)
      fullfile = site.read()
      rx = re.compile('<li>.*</li>',re.DOTALL)
      mo = rx.search(fullfile)
      if mo != None:
         substring = mo.group()
      else:
         self.keywords = []
         return 

      keyword_list = []

      hits = self.keyword_regex.findall(substring)
      for elem in hits:
         keyword_list.append(elem.strip())
     
      self.keywords = keyword_list


   def plot_summary_parser(self):
      plot_list = []

      plot_url = self.baseurl + 'plotsummary'
      site = u2.urlopen(plot_url)
      fullfile = site.read()
      hits = self.plot_summary_regex.findall(fullfile)
      for elem in hits:
         plot_list.append(elem)

      self.plot_summaries = plot_list

   def awards_parser(self):
      pass


   def updateDB(self,db):
      def toString(lst):
         if lst == []:
            return ''
         if type(lst) == types.ListType:
            return reduce(lambda x,y:'%s, %s'%(x,y),map(lambda x: str(x),lst))
         else:
            return lst

      def toStringStr(lst):
         if lst == []:
            return ''
         if type(lst) == types.ListType:
            ret_str = ''
            for elem in lst:
               ret_str + "\"" + elem + "\""
            return ret_str
         else:
            return lst

      cmd = """INSERT into fullcredits VALUES ( '%06i', '{%s}', '{%s}',\
      '{%s}', '%s', '%s', '{%s}', '{%s}', '{%s}', '{%s}', '{%s}', '{%s}', '{%s}',\
      '{%s}', '{%s}', '{%s}', '{%s}' ); """ % (self.id,\
      toString(self.fullcredits['Directed by']),\
      toString(self.fullcredits['Writing credits']),\
      toString(self.fullcredits['Cast']),\
      self.user_rating,\
      self.mpaa_rating.replace('\'','()'),\
      toString(self.fullcredits['Produced by']),\
      '',\
      toString(self.fullcredits['Cinematography by']),\
      toString(self.fullcredits['Film Editing by']),\
      '',\
      '',\
      '',\
      toStringStr(self.genres),\
      '',\
      str(self.keywords).replace('[','').replace(']','').replace('\'','\"'),\
      '')

      #print cmd
      db.query(cmd)

      cmd = """UPDATE title_id_map SET parsed = TRUE WHERE imdb_db = %06i""" % self.id
      db.query(cmd)

if __name__ == '__main__':
   db = pg.connect('imdb_project','localhost')
   id = 180093
   p = IMDbParser(id)
   p.parse()
   p.updateDB(db)
   db.close()

   
