#!/usr/bin/env python
# -*- coding:latin-1 -*-

import sys
import pg

def create_fullcredits_table(db):
   cmd = """CREATE TABLE fullcredits (
imdb_id           integer,
Directed_by       integer[],
Writing_credits   integer[],
Fullcast              integer[],
User_rating       float8,
MPAA_rating       text,
Produced_by       integer[],
Original_Music_by integer[],
Cinematography_by integer[],
Film_Editing_by   integer[],
Casting_by        integer[],
Production_Design_by integer[],
Art_Direction_by     integer[],
Genre                text[],
Country              text[],
Keywords             text[],
Plot_Summary         text[] );"""
   
   print cmd
   db.query(cmd)


def create_people_table(db):
   cmd = """CREATE TABLE people (
id integer,
name text,
first_name text,
profession text[]);"""

   db.query(cmd)
 

if __name__ == '__main__':
   db = pg.connect('imdb_project','localhost')
   #create_people_table(db)
   create_fullcredits_table(db)
   db.close()
