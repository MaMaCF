
#!/usr/bin/env python
# -*- coding: latin1 -*-

import getIMDBNumbersFromMovieTitle as g
import pg
import IMDbParser as im
import profile

try:
   import psyco
   print 'Found working psyco installation!'
   psyco.full()
except:
   print 'You do not have a working psyco installation!'


def getAllTitles(db):

   actualQuery = """SELECT imdb_db FROM title_id_map WHERE imdb_db > 0 
   AND parsed = FALSE;"""

   p = im.IMDbParser()

   for r in db.query(actualQuery).dictresult():
   
      print r
      imdb_id = '%(imdb_db)s' % r

      try:
         p.get( int(imdb_id) )
         p.parse()
         p.updateDB(db)
         print 'Successful'
      except:
         print "Not found!"


if __name__ == '__main__':
   db = pg.DB('imdb_project','localhost')
   getAllTitles(db)
   db.close()

