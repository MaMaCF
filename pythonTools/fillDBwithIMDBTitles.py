#!/usr/bin/env python
# -*- coding: latin1 -*-

import getIMDBNumbersFromMovieTitle as g
import pg

def getAllTitles(db):

   for r in db.query(
    """SELECT movie_title,production_year,netflix_id
       FROM title_id_map
       WHERE production_year <= 1989 and imdb_db=-99;"""
       ).dictresult():
   
      print r
      title = '%(movie_title)s' % r
      year = int('%(production_year)s' % r)
      title = title.replace('()','\'')
      nfx_id= int('%(netflix_id)s' % r)

      try:
         imdb_nr =  g.getMovieNumber(title,year)
      except g.IMDBQueryException:
         print "Not found!"
         imdb_nr = -77

      cmd = """UPDATE title_id_map SET imdb_db=%d WHERE netflix_id=%d;""" % (imdb_nr,nfx_id)
      db.query(cmd)

if __name__ == '__main__':
   db = pg.DB('imdb_project','localhost')
   getAllTitles(db)
   db.close()

