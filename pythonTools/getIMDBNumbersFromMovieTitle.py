#!/usr/bin/env python
# -*- coding: latin1 -*-

import urllib2 as u2
import re
import sys

class IMDBQueryException:
   pass

def getMovieNumber(title,year):
      _title = title.replace(' ','+')
      actualURL = 'http://www.imdb.com/find?s=all&q=%s' % _title
      site = u2.urlopen(actualURL)
      fullfile = site.read()

      rx = re.compile('<title>(?P<pagetitle>.*?)</title>',re.DOTALL)
      mo = rx.search(fullfile)

      if mo.group('pagetitle').startswith('IMDb'):
         rx = re.compile('.*?/title/tt0(?P<title_nr>[0-9]*)/.*?">(?P<title_string>.*?)</a>.*?\((?P<pyear>[0-9]{4,4})\)',re.DOTALL)
         mo = rx.search(fullfile)

         if mo != None: #and mo.group('title_string').strip().lower().replace('.','') == title.lower():
            print mo.group('title_nr').strip()
            print mo.group('title_string').strip()
            print mo.group('pyear').strip()
            pyear = mo.group('pyear').strip()
            if int(pyear) == year:
               return int(mo.group('title_nr').strip())
            else:
               raise IMDBQueryException
         else:
            raise IMDBQueryException

      else:
         rx = re.compile('</title>.*?href="/title/tt0(?P<nr>[0-9]+)/',re.DOTALL)
         mo = rx.search(fullfile)
         print 'NR '
         print mo.group('nr').split()
         return int(mo.group('nr').split()[0])


if __name__ == '__main__':
   fh = open(sys.argv[2],'w+')
   for line in open(sys.argv[1]):
      line = line.strip()
      if not line.startswith('#'):
         title_number = getMovieNumber(line)
         fh.write('%s, %s\n' % (line,title_number))
   fh.close()
