#!/usr/bin/env python
# -*- coding: latin1 -*-

import re
import urllib2 as u2
import stackless as st

def grabURLs(actualRange):
   global  resultFile

   print resultFile
   print 'in tasklet nr %d' % actualRange[0]

   result = ''
   for i in actualRange:
      try:
         actualURL = 'http://www.imdb.com/title/tt0%6d/' % i
         site = u2.urlopen(actualURL)
         fullfile = site.read()
         st.schedule()
         rx = re.compile('<title>(?P<movietitle>.*?)</title>',re.DOTALL)
         mo = rx.search(fullfile)
         name = mo.group('movietitle').strip().replace('&#34;','')
         result += name+'\n'
         if mo.group('movietitle').strip() == 'IMDb  Search':
            pass
      except:
         continue

   print 'Writing result...'
   resultFile.write(result)
   resultFile.flush()

def parseEntries():
   print 'Creatin tasklets...'
   incr = 10

   for i in range(400000,700000,incr):
      r = range(i,i+incr)
      st.tasklet(grabURLs)(r)

   print 'Tasklets created. Starting scheduler.'
   st.run()

   print 'Tasklets finished.'

if __name__ == '__main__':
   global resultFile
   resultFile = file('allTitles2.txt','w+')
   parseEntries()
   resultFile.close()

