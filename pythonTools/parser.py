#!/usr/bin/env python
# -*- coding:latin-1 -*-

import sys
import pg

def parse(file):
   pass
  
def createTable(db):
   cmd = 'CREATE TABLE title_id_map (\nmovie_title\
   varchar(100),\nproduction_year int,\nimdb_db int,\nnetflix_id int\n);'
   db.query(cmd)

def fill(filename,db):
   counter = 0
   for line in open(filename):
      counter += 1
      if line.startswith('#'):
         continue
      slist = line.strip().split(',',2)
      if len(slist) != 3:
         print line
         continue

      year = 0
      if slist[1] == 'NULL':
         year = 0
      else:
         year = int(slist[1])
         
      netflix_id = int(slist[0])
      title = slist[2]
      title = title.replace('\'','()')
      if len(title) > 100:
         print line
         continue

      db.query('INSERT INTO title_id_map VALUES (\'%s\', %d, %d, %d );' % (title,year,-99,netflix_id))


if __name__ == '__main__':
   db = pg.connect('imdb_project','localhost')
   db.query('DROP TABLE title_id_map')
   createTable(db)
   fill(sys.argv[1],db)
   db.close()
