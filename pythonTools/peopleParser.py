#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

import urllib2 as u2
import re
import pg
import sys

def getAllNames(db,b,e):
   for idx in range(b,e):
      url = 'http://www.imdb.com/name/nm0%06i/' % (idx)
      print url
      try:
         site = u2.urlopen(url)
         fullfile = site.read()
      except:
         print 'Could not get page nr. %d' % idx
         continue

      # get user rating
      rx = re.compile('<title>(?P<name>.*?)</title>',re.DOTALL)
      mo = rx.search(fullfile)
      if mo.group('name') != None:
         fullname = mo.group('name')
         fullname = fullname.replace('\'','()')
         fullname = fullname.replace('(I)','')
         cmd = """INSERT INTO people VALUES ( %06i, '%s', '%s', '%s' );""" %\
         (idx,fullname,'','{\'\'}')
         print cmd
         db.query(cmd)
      else:
         print 'unparseable name'

if __name__ == '__main__':
   db = pg.DB('imdb_project','localhost')
   getAllNames(db,int(sys.argv[1]),int(sys.argv[2]))
   db.close()
